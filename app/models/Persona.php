<?php

require_once "../config/BD.php";

class Persona
{
    // Atributos que usare para generar un objeto y pasarlo a las querys
    public static $attributes = ['id','cuit','apellido','nombres','email'];
    
    public static function getAll()
    {
        $sql = "SELECT * FROM personas";
        return BD::executeQuery($sql);
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM personas WHERE id = $id";
        return BD::executeQuery($sql);
    }

    public static function store($formData)
    {
        $sql="INSERT INTO personas (cuit, apellido, nombres, email) VALUES 
        ('$formData->cuit', 
        '$formData->apellido', 
        '$formData->nombres', 
        '$formData->email')";
        return BD::executeQuery($sql);
    }

    // public static function store()
    // {
    //     $all_query_ok=true; // our control variable
    //     /* deshabilitar autocommit */
    //     deshabiliteAutoCommit(false);

    //     ejecutarConsulta("INSERT INTO personas (id, cuit, apellido, nombres, email) VALUES (51, '2040043456', 'Zegray', 'Gabriel', 'gabriel@mail.com')") ? null : $all_query_ok=false;
    //     ejecutarConsulta("INSERT INTO personas (id, cuit, apellido, nombres, email) VALUES (52, '2399996663', 'Fernandez', 'Maria', 'maria@mail.com')") ? null : $all_query_ok=false;

    //     //now let's test our control variable
    //     $all_query_ok ? createCommit() : createRollback();
    //     return $all_query_ok;
    // }
}