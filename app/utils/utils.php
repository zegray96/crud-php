<?php
require '../config/BD.php';
class Utils
{
    public static function verifyMethod($requestMethod, $method)
    {
        if ($requestMethod != $method) {
            header('HTTP/1.1 405 Method not allowed');
            $array = array(
                'error' => "Metodo esperado $method"
            );
            echo json_encode($array);
            die();
        }
    }
    
    // public static function generateObject($POSTDATA)
    // {
    //     $obj = new stdClass;
    //     foreach ($POSTDATA as $key => $value) {
    //         // Si no es un array, escapamos caracteres para evitar injeccion sql
    //         if (!is_array($value)) {
    //             $value = isset($value) ? BD::cleanString($value) : "";
    //         } else {
    //             $value = isset($value) ? $value : "";
    //         }
    //         $obj->$key = $value;
    //     }
    //     return $obj;
    // }

    public static function generateObject($POSTDATA, $attributes)
    {
        $obj = new stdClass;
        foreach ($attributes as $attributesKey => $attributesValue) {
            $obj->$attributesValue = isset($POSTDATA[$attributesValue]) ? $POSTDATA[$attributesValue] : null;
        }
        return $obj;
    }

    public static function validateRequiredFields($requiredFields, $formData)
    {
        $array = array();
        foreach ($requiredFields as $keyRF => $valueRF) {
            if (!isset($formData->$valueRF) || $formData->$valueRF == "") {
                $array['errors'][]=array(
                    $valueRF => "El campo $valueRF es requerido"
                );
            }
        }

        if (!empty($array)) {
            header('HTTP/1.1 422');
            echo json_encode($array);
            die();
        }
    }

    public static function generateJsonFromMultipleResults($query)
    {
        $data = array();
        while ($reg = $query->fetch_object()) {
            $row = array();
            foreach ($reg as $regKey => $regValue) {
                $row[$regKey]= $regValue;
            }
            $data[]=$row;
        }
        return json_encode($data);
    }

    public static function generateJsonFromUniqueResult($query)
    {
        $data = null;
        if ($query) {
            $data = $query->fetch_assoc();
        }
        return json_encode($data);
    }
}