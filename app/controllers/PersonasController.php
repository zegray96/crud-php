<?php
// Para que retorne en formato json
header("Content-Type: application/json");

require '../utils/Utils.php';
/** Modelos que utilizaremos */
require '../models/Persona.php';
/** End Modelos */

$method = $_SERVER['REQUEST_METHOD'];
$function = $_REQUEST['function'];

switch ($function) {
    // GET
    case 'getAll':
        Utils::verifyMethod($method, "GET");

        $result = Persona::getAll();
        $result = Utils::generateJsonFromMultipleResults($result);

        echo $result;
        break;

    // GET
    case 'getById':
        Utils::verifyMethod($method, "GET");
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        
        $result = Persona::getById($id);

        $result = Utils::generateJsonFromUniqueResult($result);

        echo $result;
        break;

    // POST
    case 'store':
        Utils::verifyMethod($method, "POST");

        // Generamos el objeto para pasarlo a la consulta
        $formData = Utils::generateObject($_POST, Persona::$attributes);

        // Campos requeridos
        $requiredFields = array(
            'cuit',
            'nombres',
            'apellido'
        );
        Utils::validateRequiredFields($requiredFields, $formData);

        // Ejecutamos la query
        $result = Persona::store($formData);

        // Retornamos resultado
        if ($result) {
            $result = array(
                'message' => "¡Registro creado con exito!"
            );
        } else {
            $result = array(
                'message' => "¡Registro no se pudo crear!"
            );
        }
        
        echo json_encode($result);
        break;

    // DELETE
    case 'delete':
        Utils::verifyMethod($method, "DELETE");
        $id = $_REQUEST['id'];
        
        echo json_encode($id);
        break;
    
    default:
        header('HTTP/1.1 404');
        $error = array(
            'error' => "Funcion no encontrada"
        );
        echo json_encode($error);
        break;
}