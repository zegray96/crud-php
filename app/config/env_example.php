﻿<?php
//Servidor de base de datos
define("DB_HOST", "localhost");

//Nombre de la base de datos
define("DB_NAME", "");

//Usuario de la base de datos
define("DB_USERNAME", "");

//Clave del usuario de la base de datos
define("DB_PASSWORD", "");

//Codificacion de los caracteres
define("DB_ENCODE", "utf8");

//Nombre del proyecto
define("PRO_NOMBRE", "");

//Version del proyecto
define("VERSION", "1.0");

//Dominio
define("DOMAIN", "https://google.com.ar");