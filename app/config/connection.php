<?php

require_once "env.php"; //con once que si ya esta incluido que no lo incluya

$conexion=new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

mysqli_query($conexion, 'SET NAMES "'.DB_ENCODE.'"'); //hago una consulta para que la codificacion de caractares sea lo que puse en db encode

//Si tenemos un error en la conexxion nos dira esto
if (mysqli_connect_errno()) {
    printf("Fallo conexion a la BD: %s \n", mysqli_connect_error());
    exit();
}

if (!function_exists('ejecutarConsulta')) {
    function ejecutarConsulta($sql)
    {
        global $conexion;
        $query=$conexion->query($sql);
        return $query;
    }


    function ejecutarConsultaSimpleFila($sql)
    {
        global $conexion;
        $query=$conexion->query($sql);
        $row = $query->fetch_assoc();
        return $row;
    }

    function ejecutarConsulta_retornarID($sql)
    {
        global $conexion;
        $query=$conexion->query($sql);
        return $conexion->insert_id;
    }

    function limpiarCadena($str)
    {
        global $conexion;
        $str=mysqli_real_escape_string($conexion, trim($str));
        return htmlspecialchars($str);
    }

    function deshabiliteAutoCommit()
    {
        global $conexion;
        $conexion->autocommit(false);
    }

    function createCommit()
    {
        global $conexion;
        $conexion->commit();
    }

    function createRollback()
    {
        global $conexion;
        $conexion->rollback();
    }
}
