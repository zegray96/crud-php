<?php
require_once "env.php"; //con once que si ya esta incluido que no lo incluya

class BD
{
    public static $conexion;

    public static function openConnection()
    {
        self::$conexion=new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        //Si tenemos un error en la conexion nos dira esto
        if (mysqli_connect_errno()) {
            printf("Fallo conexion a la BD: %s \n", mysqli_connect_error());
            exit();
        }
    }

    
    public function cleanString($str)
    {
        self::openConnection();
        $str=mysqli_real_escape_string(self::$conexion, trim($str));
        return htmlspecialchars($str);
    }

    public function executeQuery($sql)
    {
        self::openConnection();
        $query=self::$conexion->query($sql);
        return $query;
    }

    public function executeQueryReturnId($sql)
    {
        self::openConnection();
        self::$conexion->query($sql);
        $id = self::$conexion->insert_id;
        return $id;
    }

    public function deshabiliteAutoCommit()
    {
        self::openConnection();
        self::$conexion->autocommit(false);
    }

    public function createCommit()
    {
        self::openConnection();
        self::$conexion->commit();
    }

    public function createRollback()
    {
        self::openConnection();
        self::$conexion->rollback();
    }
}
