-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-05-2022 a las 16:00:46
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crud_php`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` bigint(20) NOT NULL,
  `cuit` varchar(11) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cuit`, `apellido`, `nombres`, `email`) VALUES
(1, '77-942-8472', 'Drewet', 'Gerik', 'gdrewet0@blog.com'),
(2, '36-778-1165', 'MacCleay', 'Glenn', 'gmaccleay1@oracle.com'),
(3, '09-966-7914', 'Bewshaw', 'Fanchon', 'fbewshaw2@feedburner.com'),
(4, '69-122-1034', 'Charlton', 'Callean', 'ccharlton3@ca.gov'),
(5, '32-558-4274', 'Bilt', 'Gus', 'gbilt4@tripadvisor.com'),
(6, '98-684-2856', 'Laweles', 'Ezri', 'elaweles5@over-blog.com'),
(7, '57-161-9694', 'Segrott', 'Cassandry', 'csegrott6@sitemeter.com'),
(8, '77-040-8148', 'Padginton', 'Brade', 'bpadginton7@youku.com'),
(9, '68-043-7328', 'Witherup', 'Rex', 'rwitherup8@artisteer.com'),
(10, '89-977-9212', 'Ezzy', 'Sawyere', 'sezzy9@upenn.edu'),
(11, '93-977-9843', 'Rodie', 'Valaree', 'vrodiea@last.fm'),
(12, '67-909-4700', 'Baldery', 'Quinta', 'qbalderyb@ezinearticles.com'),
(13, '03-227-7116', 'Swanton', 'Roby', 'rswantonc@moonfruit.com'),
(14, '14-701-6968', 'Linny', 'Cherlyn', 'clinnyd@nyu.edu'),
(15, '12-598-3397', 'Ollerton', 'Bordie', 'bollertone@tiny.cc'),
(16, '82-979-7781', 'Sayer', 'Sharlene', 'ssayerf@ow.ly'),
(17, '15-720-9798', 'Heinreich', 'Karrah', 'kheinreichg@eepurl.com'),
(18, '59-326-6432', 'Rochell', 'Cher', 'crochellh@ovh.net'),
(19, '05-593-0820', 'Oram', 'Galen', 'gorami@wsj.com'),
(20, '33-142-8226', 'Karpeev', 'Filbert', 'fkarpeevj@cisco.com'),
(21, '58-295-5833', 'Berrigan', 'Lisbeth', 'lberrigank@alibaba.com'),
(22, '36-796-6959', 'Ravenshear', 'Leta', 'lravenshearl@example.com'),
(23, '90-271-7603', 'Wildes', 'Abbie', 'awildesm@about.me'),
(24, '75-087-8251', 'Medlen', 'Phylis', 'pmedlenn@businesswire.com'),
(25, '21-591-9290', 'Handasyde', 'Meridel', 'mhandasydeo@earthlink.net'),
(26, '86-041-0559', 'Kimble', 'Karen', 'kkimblep@yolasite.com'),
(27, '11-336-4810', 'Edgerly', 'Giordano', 'gedgerlyq@hibu.com'),
(28, '91-180-5619', 'MacTrustie', 'Mariette', 'mmactrustier@independent.co.uk'),
(29, '33-091-8491', 'Easby', 'Merwin', 'measbys@about.me'),
(30, '92-530-0805', 'Malcolm', 'Vachel', 'vmalcolmt@paypal.com'),
(31, '04-360-9356', 'Goodyer', 'Stephi', 'sgoodyeru@va.gov'),
(32, '63-849-5925', 'Raspin', 'Nappy', 'nraspinv@hatena.ne.jp'),
(33, '15-289-3489', 'McGillreich', 'Elsey', 'emcgillreichw@who.int'),
(34, '31-116-7498', 'Yersin', 'Thekla', 'tyersinx@irs.gov'),
(35, '28-971-9675', 'Baddeley', 'Mervin', 'mbaddeleyy@hc360.com'),
(36, '81-674-1649', 'Bogaert', 'Rosemarie', 'rbogaertz@oracle.com'),
(37, '89-494-0133', 'Tytler', 'Madelyn', 'mtytler10@hatena.ne.jp'),
(38, '92-257-2160', 'Dodgson', 'Caroline', 'cdodgson11@behance.net'),
(39, '25-602-5219', 'Dundon', 'Rock', 'rdundon12@tripod.com'),
(40, '23-811-8371', 'Hinstock', 'Lauritz', 'lhinstock13@wordpress.com'),
(41, '24-312-0667', 'Guyan', 'Maurits', 'mguyan14@hatena.ne.jp'),
(42, '83-461-4242', 'Falconbridge', 'Gregg', 'gfalconbridge15@discovery.com'),
(43, '52-565-2056', 'Hamson', 'Olivia', 'ohamson16@home.pl'),
(44, '40-221-7732', 'Cregin', 'Delano', 'dcregin17@godaddy.com'),
(45, '56-414-8920', 'Fasson', 'Rosemonde', 'rfasson18@amazon.co.jp'),
(46, '31-349-5548', 'Jessen', 'Nanon', 'njessen19@instagram.com'),
(47, '52-224-3376', 'Shinton', 'Albert', 'ashinton1a@plala.or.jp'),
(48, '62-705-3048', 'Giff', 'Minnie', 'mgiff1b@artisteer.com'),
(49, '35-821-3316', 'Lodewick', 'Jorrie', 'jlodewick1c@about.me'),
(50, '75-136-5153', 'Plose', 'Opalina', 'oplose1d@creativecommons.org');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
