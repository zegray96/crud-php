const form = document.getElementById('form');

form.addEventListener("submit", function (e) {
    e.preventDefault();
    storePersona();
});

const getPersonas = () => {
    axios({
        method: 'GET',
        url: '../app/controllers/PersonasController',
        params: {
            function: 'getAll'
        }
    })
        .then(function (response) {
            console.log(response.data);
        })
        .catch(function (err) {
            console.log(err.response.data);
        });
}

const storePersona = () => {
    let formData = new FormData(form);
    axios({
        method: 'POST',
        url: '../app/controllers/PersonasController',
        params: {
            function: 'store'
        },
        data: formData
    })
        .then(function (response) {
            console.log(response.data);
        })
        .catch(function (err) {
            console.log(err);
        });
}


const deletePersona = () => {
    axios({
        method: 'DELETE',
        url: '../app/controllers/PersonasController',
        params: {
            function: 'delete',
            id: 10
        }
    })
        .then(function (response) {
            console.log(response.data);
        })
        .catch(function (err) {
            console.log(err);
        });
}


