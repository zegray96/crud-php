<?php require('../app/config/env.php');?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php require('layouts/links.php') ?>

    <title><?php echo PRO_NOMBRE; ?> | Home</title>
</head>

<body>
    <?php require('layouts/nav.php');?>

    <main class="container-md">
        <!-- CARDS -->
        <div class="row mt-5">
            <div class="col-sm-4">
                <a href="personas">
                    <div class="card text-white bg-primary mb-3">
                        <div class="card-header"> Personas</div>
                        <div class="card-body">
                            <h2 class="card-title"><i class="fa fa-users"></i></h2>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!-- END CARDS -->
    </main>

    <?php require('layouts/scripts.php')?>

</body>

</html>