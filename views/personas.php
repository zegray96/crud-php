<?php require('../app/config/env.php');?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php require('layouts/links.php') ?>

    <title><?php echo PRO_NOMBRE; ?> | Personas</title>
</head>

<body>
    <div>
        <button type="button" onclick="getPersonas()">Listar personas</button>
    </div>

    <div>
        <button type="button" onclick="deletePersona()">Delete persona</button>
    </div>

    <div>
        <form id="form">
            <label for="">Nombre</label>
            <input type="text" name="nombre">
            <button type="submit">Enviar</button>
        </form>
    </div>

    <?php require('layouts/scripts.php')?>

    <script src="../js/personas.js"></script>
</body>

</html>